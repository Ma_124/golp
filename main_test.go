package golp

import (
	"testing"
	"github.com/spf13/afero"
	"os"
	"os/exec"
)

const melddiff = true

func TestGolp_Execute(t *testing.T) {
	outFs := afero.NewMemMapFs()
	tasks := map[string]TaskFunc{
		"deprecated_pipeStdio_sed": func(i I) {
			i.Source("test/deprecated_pipeStdio_sed.txt").
				Pipe(pipeStdio("sed", "s/Golp/World/g")).
				Dest("test_out")
		},
		"pipeCommandExecStdio_sed": func(i I) {
			i.Source("test/pipeCommandExecStdio_sed.txt").
				Pipe(PipeCommandExecStdio("sed", "s/Foo/Bar/g")).
				Dest("test_out")
		},
		"pipeCommandExecFiles_ex": func(i I) {
			i.Source("test/pipeCommandExecFiles_ex.txt").
				Pipe(PipeCommandExecFile("/tmp/test_golp_pipeCommandExecFiles_ex.txt", "ex", "-s", "-c", `%s/World/Golp!/g|x`, "/tmp/test_golp_pipeCommandExecFiles_ex.txt")).
				Dest("test_out")
		},
	}
	g := &G{
		tasks,
		afero.NewOsFs(),
		outFs,
		afero.NewBasePathFs(outFs, ".cache"),
	}

	suc := true

	for task := range tasks {
		errs := g.Execute(&GArgs{
			Tasks: []string{task},
		})

		if errs != nil {
			for _, err := range errs {
				t.Log(err)
			}
			suc = false
		}
	}

	suc = suc && assertFile(t, g.OutFs, "deprecated_pipeStdio_sed.txt", "Hello World!")
	suc = suc && assertFile(t, g.OutFs, "pipeCommandExecFiles_ex.txt", "Hello Golp!\n")
	suc = suc && assertFile(t, g.OutFs, "pipeCommandExecStdio_sed.txt", "Hello Bar!")
	if !suc {
		t.FailNow()
	}
}

func assertFile(t *testing.T, outFs afero.Fs, fname string, expected string) bool {
	data, err := afero.ReadFile(outFs, "test_out/" + fname)
	if err != nil {
		t.Log(err)
		return false
	}
	if string(data) != expected {
		if melddiff {
			f, err := os.Create("/tmp/golp_test_got")
			if err != nil {
				t.Log(err)
			}
			f.Write(data)
			f.Close()

			f, err = os.Create("/tmp/golp_test_expected")
			if err != nil {
				t.Log(err)
			}
			f.Write([]byte(expected))
			f.Close()

			exec.Command("meld", "/tmp/golp_test_got", "/tmp/golp_test_expected").Run()
		}
		t.Log(fname + ": data != expected")
		t.Log(string(data))
		t.Log("------------------")
		t.Log(expected)
		t.Log("==================")
		return false
	}
	t.Log(fname + ": successful")
	return true
}
