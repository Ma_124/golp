package golp

import (
	"os/exec"
	"github.com/spf13/afero"
	"io"
	"os"
	"bytes"
)

func PipeStreams(in func(state interface{})(io.WriteCloser, error), out func(state interface{})(io.ReadCloser, error), state interface{}, pre func(i I, intname string, state interface{}), during func(i I, intname string, state interface{}), post func(i I, intname string, state interface{})) PipeFunc {
	return func(i I, intname string) {
		if pre != nil {
			pre(i, intname, state)
		}

		data, err := afero.ReadFile(i.Fs(), intname)
		if err != nil {
			i.AddError(err)
			return
		}
		inw, err := in(state)
		if err != nil {
			i.AddError(err)
			return
		}
		defer inw.Close()
		inw.Write(data)
		inw.Close()

		if during != nil {
			during(i, intname, state)
		}

		outw, err := out(state)
		if err != nil {
			i.AddError(err)
			return
		}
		defer outw.Close()

		data, err = afero.ReadAll(outw)
		if err != nil {
			i.AddError(err)
			return
		}
		outw.Close()

		f, err := i.Fs().Create(intname)
		if err != nil {
			i.AddError(err)
			return
		}
		defer f.Close()
		f.Write(data)

		if post != nil {
			post(i, intname, state)
		}
	}
}

type pipeCommandExecState struct {
	Cmd *exec.Cmd
}

func PipeCommandExec(in func(state interface{})(io.WriteCloser, error), out func(state interface{})(io.ReadCloser, error), name string, args ...string) PipeFunc {
	return PipeStreams(in, out, &pipeCommandExecState{}, func(i I, intname string, state interface{}) {
		state.(*pipeCommandExecState).Cmd = exec.Command(name, args...)
	}, func(i I, intname string, state interface{}) {
		state.(*pipeCommandExecState).Cmd.Run()
	}, nil)
}

func PipeCommandExecFiles(in, out string, name string, args ...string) PipeFunc {
	return PipeCommandExec(func(state interface{}) (io.WriteCloser, error) {
		return os.Create(in)
	}, func(state interface{}) (io.ReadCloser, error) {
		return os.Open(out)
	}, name, args...)
}

func PipeCommandExecFile(f string, name string, args ...string) PipeFunc {
	return PipeCommandExecFiles(f, f, name, args...)
}

// temporary implementation for PipeCommandExecStdio
func pipeStdio(name string, args ...string) PipeFunc {
	return func(i I, intname string) {
		data, err := afero.ReadFile(i.Fs(), intname)
		if err != nil {
			i.AddError(err)
			return
		}

		cmd := exec.Command(name, args...)

		f, err := i.Fs().Create(intname)
		if err != nil {
			i.AddError(err)
			return
		}
		defer f.Close()

		cmd.Stdout = f
		cmd.Stdin = bytes.NewReader(data)

		err = cmd.Run()
		if err != nil {
			i.AddError(err)
			return
		}
	}
}

func PipeCommandExecStdio(name string, args ...string) PipeFunc {
	return pipeStdio(name, args...) // TODO PipeCommandExec implementation
}

// TODO PipeCommandExecStdin, PipeCommandExecStdout

type readNopCloser struct {
	io.Reader
}

func (readNopCloser) Close() error {
	return nil
}

func NewReadNoopCloser(r io.Reader) io.ReadCloser {
	if r == nil { panic("nil reader") }
	return &readNopCloser{ r }
}

type writeNopCloser struct {
	io.Writer
}

func (writeNopCloser) Close() error {
	return nil
}

func NewWriteNoopCloser(w io.Writer) io.WriteCloser {
	if w == nil { panic("nil writer") }
	return &writeNopCloser{ w }
}
