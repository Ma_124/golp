package golp

import (
	"os"
	"github.com/mkideal/cli"
)

type argT struct {

}

func Golpfile(task map[string]TaskFunc) {
	os.Exit(cli.Run(new(argT), func(ctx *cli.Context) error {
		argv := ctx.Argv().(*argT)

		gargs := &GArgs{
			Tasks: ctx.Args(),
		}

		NewOsG(task).Execute(gargs)
		return nil
	}))
}