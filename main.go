package golp

import (
	"os"
	"github.com/spf13/afero"
)

type TaskFunc func(i I)

type GArgs struct {
	Tasks []string
}

type G struct {
	Tasks map[string]TaskFunc
	InFs  afero.Fs
	OutFs afero.Fs
	CacheFs afero.Fs
}

func NewOsG(tasks map[string]TaskFunc) *G {
	fs := afero.NewOsFs()
	fs.MkdirAll(".golp-cache", os.ModePerm)
	return &G{
		Tasks: tasks,
		InFs: fs,
		OutFs: fs,
		CacheFs: afero.NewBasePathFs(fs, ".golp-cache"),
	}
}

func (g *G) Execute(args *GArgs) []error {
	errs := make([]error, 0)

	for _, task := range args.Tasks {
		i := NewMemMapI(g)
		g.Tasks[task](i)

		errs = append(errs, i.Errors()...)
	}

	if len(errs) == 0 {
		return nil
	}
	return errs
}
