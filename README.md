# Golp
> *Inspired by [Gulp](https://gulpjs.com/) but without JS*

```go
//go run $@; exit $?
package main

import (
    g "gitlab.com/Ma_124/golp"
    "gitlab.com/Ma_124/glop-sass"
)

func main() {
    g.Golpfile(map[string]g.TaskFunc{
        "build": func(i g.I) {
            i.Source("src/**/*.scss").
                Pipe(golpsass.scss).
                Dest("public/style")
        },
    })
}
```
