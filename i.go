package golp

import (
	"github.com/spf13/afero"
	"github.com/satori/go.uuid"
	"path/filepath"
	"os"
)

type PipeFunc func(i I, intname string)

type I interface {
	Golp() *G

	Source(globs ...string) I
	SourceFiles(fs ...string) I
	Dest(path string) I

	Pipe(f PipeFunc) I

	Errors() []error
	AddError(err error) I

	FileName(fn string) (fname string, id string, intname string)

	Fs() afero.Fs

	SetFileMeta(k string, f *F)
	GetFileMeta(k string) *F
}

type i struct {
	g *G
	fs afero.Fs
	FileMeta map[string]*F
	errs []error
}

type F struct {
	OriginalName string
}

func NewI(g *G, fs afero.Fs) I {
	return &i{
		g, fs, map[string]*F{}, []error{},
	}
}

func NewMemMapI(g *G) I {
	return NewI(g, afero.NewMemMapFs())
}

func (i *i) Fs() afero.Fs {
	return i.fs
}

func (i *i) Source(globs ...string) I {
	for _, g := range globs {
		ifs, err := afero.Glob(i.Golp().InFs, g)

		if err == nil {
			i.SourceFiles(ifs...)
		} else {
			i.AddError(err)
		}
	}

	return i
}

func (i *i) Pipe(f PipeFunc) I {
	i.pipeDir(".", f)
	return i
}

func (i *i) pipeDir(dir string, f PipeFunc) {
	fis, err := afero.ReadDir(i.Fs(), dir)
	if err != nil {
		i.AddError(err)
		return
	}
	for _, fi := range fis {
		if fi.IsDir() {
			i.pipeDir(filepath.Join(dir, fi.Name()), f)
		} else {
			f(i, filepath.Join(dir, fi.Name()))
		}
	}
}

func (i *i) Dest(path string) I {
	i.destDir(".", path)
	return i
}

func (i *i) destDir(dirpath, path string) {
	i.Golp().OutFs.MkdirAll(filepath.Join(path, dirpath), os.ModePerm)

	fis, err := afero.ReadDir(i.Fs(), dirpath)
	if err != nil {
		i.AddError(err)
		return
	}

	for _, fi := range fis {
		if fi.IsDir() {
			i.destDir(fi.Name(), path)
		} else {
			i.destFile(filepath.Join(dirpath, fi.Name()), path)
		}
	}
}

func (i *i) destFile(intpath, outDir string) {
	data, err := afero.ReadFile(i.Fs(), intpath)
	if err != nil {
		i.AddError(err)
		return
	}

	fn := intpath[:len(intpath)-37]
	f, err := i.Golp().OutFs.Create(filepath.Join(outDir, fn))
	if err != nil {
		i.AddError(err)
		return
	}
	defer f.Close()
	f.Write(data)
}

func (i *i) Golp() *G {
	return i.g
}

func (i *i) SourceFiles(fs ...string) I {
	for _, f := range fs {
		i.SourceFile(f)
	}

	return i
}

func (i *i) SourceFile(fn string) I {
	_, id, intname := i.FileName(fn)

	data, err := afero.ReadFile(i.Golp().InFs, fn)
	if err != nil {
		return i.AddError(err)
	}

	f, err := i.Fs().Create(intname)
	if err != nil {
		return i.AddError(err)
	}
	defer f.Close()

	_, err = f.Write(data)
	if err != nil {
		return i.AddError(err)
	}

	i.FileMeta[id] = &F{
		OriginalName: fn,
	}

	return i
}

func (i *i) FileName(fn string) (fname string, id string, intname string) {
	for {
		ido, _ := uuid.NewV4()
		id = ido.String()

		// Anti collision
		if found, _ := afero.Exists(i.Fs(), fn + "_" + id); !found {
			break
		}
	}

	_, nfn := filepath.Split(fn) // TODO don't discard the dir
	return nfn, id, nfn + "_" + id
}

func (i *i) Errors() []error {
	return i.errs
}

func (i *i) AddError(err error) I {
	panic(err)
	i.errs = append(i.errs, err)
	return i
}

func (i *i) SetFileMeta(k string, f *F) {
	i.FileMeta[k] = f
}

func (i *i) GetFileMeta(k string) *F {
	return i.FileMeta[k]
}
